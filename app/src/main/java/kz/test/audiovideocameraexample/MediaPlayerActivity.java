package kz.test.audiovideocameraexample;

import android.Manifest;
import android.content.ContentUris;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MediaPlayerActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    private String DATA_HTTP = "https://kwmusic.xn--41a.wiki/public/download_song.php?link=aHR0cHM6Ly9zMy54bi0tNDFhLndpa2kvMS82MzY2XzM4NzNkZGIyYjJlN2QxN2I3YjNiNjlkZTBmNDY5MDcyLm1wMz9maWxlbmFtZT1hcnRpay1hc3RpXy1feWEtdHZveWEubXAz";
    private String DATA_STREAM = "http://online.radiorecord.ru:8101/rr_128";
    private String DATA_SD = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC) + "/music.mp3";
    private Uri DATA_URI = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, 13359);

    private MediaPlayer mediaPlayer;
    private CheckBox loop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        ActivityCompat.requestPermissions(
                MediaPlayerActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                100
        );

        loop = findViewById(R.id.loop);
        loop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mediaPlayer != null) {
                    mediaPlayer.setLooping(isChecked);
                }
            }
        });
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d("Hello", "onPrepared");
        Toast.makeText(MediaPlayerActivity.this, "Track completed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d("Hello", "onPrepared");
        mp.start();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    public void onClick(View view) {
        releaseMP();

        try {
            switch (view.getId()) {
                case R.id.httpButton:
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(DATA_HTTP);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setOnPreparedListener(this);
                    mediaPlayer.prepareAsync();
                    break;
                case R.id.streamButton:
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(DATA_STREAM);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setOnPreparedListener(this);
                    mediaPlayer.prepareAsync();
                    break;
                case R.id.sdButton:
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(DATA_SD);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    break;
                case R.id.uriButton:
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(MediaPlayerActivity.this, DATA_URI);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    break;
                case R.id.rawButton:
                    mediaPlayer = MediaPlayer.create(MediaPlayerActivity.this, R.raw.explosion);
                    mediaPlayer.start();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mediaPlayer == null) {
            return;
        }
        mediaPlayer.setLooping(loop.isChecked());
    }

    private void releaseMP() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onClickAction(View view) {
        if (mediaPlayer == null) {
            return;
        }
        switch (view.getId()) {
            case R.id.playButton:
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;
            case R.id.pauseButton:
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                }
                break;
            case R.id.stopButton:
                mediaPlayer.stop();
                break;
            case R.id.backwardButton:
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 2000);
                break;
            case R.id.forwardButton:
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 2000);
                break;
        }
    }
}