package kz.test.audiovideocameraexample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private File directory;

    private final int REQUEST_PHOTO = 100;
    private final int REQUEST_VIDEO = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);

        directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "Pictures");
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    public void mediaPlayer(View view) {
        startActivity(new Intent(MainActivity.this, MediaPlayerActivity.class));
    }

    public void mediaRecocder(View view) {
        startActivity(new Intent(MainActivity.this, MediaRecorderActivity.class));
    }

    public void soundPool(View view) {
        startActivity(new Intent(MainActivity.this, SoundPoolActivity.class));
    }

    public void recordVideo(View view) {
        startActivity(new Intent(MainActivity.this, RecordVideoActivity.class));
    }

    public void openCameraForPhoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, generateFileUri(1));
        startActivityForResult(intent, REQUEST_PHOTO);
    }

    public void openCameraForVideo(View view) {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, generateFileUri(2));
        startActivityForResult(intent, REQUEST_VIDEO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PHOTO) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    Bitmap bitmap = (Bitmap) bundle.get("data");
                    imageView.setImageBitmap(bitmap);
                }
            } else if (requestCode == REQUEST_VIDEO) {
                if (data != null) {
                    Toast.makeText(MainActivity.this, "Uri:" + data.getData(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private Uri generateFileUri(int type) {
        File file = null;
        if (type == 1) {
            file = new File(directory.getPath() + "/photo_" + System.currentTimeMillis() + ".jpg");
        } else {
            file = new File(directory.getParent() + "/video_" + System.currentTimeMillis() + ".mp4");
        }
        return Uri.fromFile(file);
    }
}