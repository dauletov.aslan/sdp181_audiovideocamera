package kz.test.audiovideocameraexample;

import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class SoundPoolActivity extends AppCompatActivity {

    private int MAX_STREAMS = 2;
    private SoundPool soundPool;
    private int soundIdShot, soundIdExplosion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sount_pool);

        SoundPool.Builder soundPoolBuilder = new SoundPool.Builder();
        soundPoolBuilder.setMaxStreams(MAX_STREAMS);
        soundPool = soundPoolBuilder.build();

        soundIdShot = soundPool.load(SoundPoolActivity.this, R.raw.shot, 1);
        try {
            soundIdExplosion = soundPool.load(getAssets().openFd("explosion.ogg"), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play(View view) {
        soundPool.play(soundIdShot, 1, 1, 0, 10, 2);
        soundPool.play(soundIdExplosion, 1, 1, 0, 1, 0.5f);
    }
}